var feat_posts_ids   = [ "template_post", "test_post_1"];
var pop_posts_ids    = [ "test_post_1", "test_post_2" ];
var spon_posts_ids   = [ "template_post", "test_post_1", "test_post_2" ];
var latest_posts_ids = [ "template_post", "test_post_1", "test_post_2" ];

var posts =
    {
        "template_post":
        {
            title_id: "template_post",
            route: "/posts/template_post",
            title: "Template post",
            summary: "Lorem ipsum dolor amet hella PBR&B ramps bitters. Blue bottle church-k",
            author: "Anonymous",
            img: "/template_post/img/local_000000.png",
            created: new Date('2017-08-15T00:00:00Z'),
            updated: new Date('2018-07-07T00:00:00Z'),
            read_time: "unknown",
            categories: ["Anon"],
            tags: ["Anon tag"],
            social_links: {
                "twitter": "https://twitter.com",
                "facebook": "https://www.facebook.com",
                "instagram": "https://www.instagram.com"
            }
        },


        "test_post_1":
        {
            title_id: "test_post",
            route: "/posts/test_post",
            title: "Test post 1",
            summary: "und dreamcatcher tilde asymmetrical vaporware. Cred wayfarers +1 cardigan try-hard direct trade ",
            author: "Pepe Chavez",
            img: "https://via.placeholder.com/1080x720/737373.png",
            created: new Date('2017-12-12T00:00:00Z'),
            updated: new Date('2018-05-10T00:00:00Z'),
            read_time: "10 min",
            categories: ["review"],
            tags: ["tag 0", "longer tag 1", "tag 2", "another tag 3"],
            social_links: {
                "twitter": "",
                "facebook": "",
                "instagram": ""
            }
        },


        "test_post_2":
        {
            title_id: "test_post_2",
            route: "/posts/test_post_2",
            title: "Test post 2",
            summary: "3 wolf moon franzen cloud bread fingerstache. Succulents gentrify retro cray godard shore",
            author: "Jose Sandoval",
            img: "https://via.placeholder.com/1080x720/F2F2F2.png",
            created: new Date('2017-10-07T00:00:00Z'),
            updated: new Date('2018-06-15T00:00:00Z'),
            read_time: "15 min",
            categories: ["howto", "review"],
            tags: ["tag 0", "longer tag 1", "tag 2", "another tag 3"],
            social_links: {
                "twitter": "",
                "facebook": "",
                "instagram": ""
            }
        }


    };

exports.posts = posts;
exports.feat_posts_ids  = feat_posts_ids;
exports.pop_posts_ids   = pop_posts_ids;
exports.spon_posts_ids  = spon_posts_ids;
exports.latest_posts_ids = latest_posts_ids;