# Guru de Inversiones

## TODOs

1. multilanguage support
2. search box with suggestions (dependant of multilanguage support)
3. social media share buttons functionality
    - https://www.addthis.com
4. Select font and font sizes (currently using Bootstrap's default)


## Dev and Test Setup

### Requirements

- [git](https://git-scm.com)
- [NodeJS](https://nodejs.org/en/)
- [NPM](https://docs.npmjs.com)
- [MongoDB](https://docs.mongodb.com)
    - **MongoDB setup is only needed if working in `database_blog` branch or other branch that uses MongoDB**. You can skip MongoDB install section and Run/Test step 1 if working on master

#### git

1.- Install git
```
sudo apt-get install git
```
2.- Get/Clone `guru-de-inversiones` repo
```
git clone git@bitbucket.org:guruinvteam/guru-de-inversiones.git
```

#### NodeJS and NPM

1.- [Install NodeJS](https://nodejs.org/en/download/package-manager/):
```
sudo apt-get update
sudo apt-get install -y nodejs
```

2.- When you install node.js, npm is automatically installed so just check NodeJS and NPM versions and that they were installed correctly
```
node -v
npm -v
```

> Make sure you have the latest version of `npm`, you can check this at the end of [NPM Getting Started Webpage](https://docs.npmjs.com/getting-started/installing-node) and use `npm install npm@latest -g` to upgrade `npm`


#### MongoDB

1.- Install Mongo:
```
sudo apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv 9DA31620334BD75D9DCB49F368818C72E52529D4
echo "deb [ arch=amd64 ] https://repo.mongodb.org/apt/ubuntu trusty/mongodb-org/4.0 multiverse" | sudo tee /etc/apt/sources.list.d/mongodb-org-4.0.list
sudo apt-get update
sudo apt-get install -y mongodb-org
```

> Check [MongoDB Install Reference](https://docs.mongodb.com/master/tutorial/install-mongodb-on-ubuntu/) and give preference over instructions here


### Run/Test

1.- Run MongoDB use `mongod` script in `guru-de-inversiones/mongo_data/` folder and leave it running on a terminal
```
cd guru-de-inversiones/mongo_data/
./mongod
```

> Make sure `guru-de-inversiones/mongo_data/data` folder exists and `mongod` script contains `mongod --dbpath=./data --nojournal`

2.- On a new terminal run `app.js` using `node`
```
cd guru-de-inversiones/
node app.js
```

3.- If everything is OK you should see a message in the terminal stating the server has started. Ex:
```
~/workspace/ $ cd guru-de-inversiones/
~/workspace/guru-de-inversiones/ (master) $ node app.js
Guru de Inversiones Server Started...
```

> Make sure to kill (Ctrl-C) the `mongod` script and the server when is not needed


### npm installs
Document here all the `npm` packages that were installed in the project just as a historic reference, check package.json for the version used of each package
```
npm install express mongoose ejs express-sanitizer body-parser --save
npm install passport passport-local passport-local-mongoose express-session --save
```