// Search database for typeahead's suggestions
function search(query, syncResults, asyncResults) {
    // Get places matching query (asynchronously)
    let parameters = {
        q: query
    };

    //console.log("called search with query= " + query);

    $.getJSON("/suggestions", parameters, function(data, textStatus, jqXHR) {
        // console.log(data);
        // Call typeahead's callback with search results (i.e., places)
        asyncResults(data);
    });
}


function init_search_bar(sel_str){
    // Configure typeahead
    $(sel_str).typeahead({
        highlight: false,
        minLength: 1
    },
    {
        display: function(suggestion) { return null; },
        limit: 5,
        source: search,
        templates: {
            suggestion: Handlebars.compile(
                "<div>" +
                "<a href=\"{{route}}\">" +
                "<img width=\"30px\" height=\"30px\" src =\"{{img}}\">" +
                "<span> {{title}}</span>" +
                "</a>" +
                "</div>"
            )
        }
    });
}