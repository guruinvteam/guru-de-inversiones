
 //https://stackoverflow.com/questions/31561717/bootstrap-carousel-indicators-out-of-the-main-div-not-switching-automatically
 $( document ).ready(function() {

    var carousel = $('#carouselExampleIndicators');
    var card_title = $('#featured_card_title');
    var card_text = $('#featured_card_text');
    var anchors = $('.featured_card_body a , #active_carousel_item');

    init_carousel();
    init_search_bar("#search-input");


function init_carousel() {
    var sel_carousel_item = $('.carousel-inner div:nth-child(1)');
    var link = sel_carousel_item.attr("data-link");
    var title = sel_carousel_item.attr("data-title");
    var summary = sel_carousel_item.attr("data-summary").substring(0, 70);

    card_title.text(title);
    card_text.text(summary);
    anchors.attr("href", link);

    carousel.on('slide.bs.carousel', function (event) {
        var indx = Number(event.to);

        $('.carousel-indicators li').removeClass('active');
        $('.carousel-indicators li:nth-child('+(indx+1)+')').addClass('active');

        var sel_carousel_item = $('.carousel-inner div:nth-child('+(indx+1)+')');
        var link = sel_carousel_item.attr("data-link");
        var title = sel_carousel_item.attr("data-title");
        var summary = sel_carousel_item.attr("data-summary").substring(0, 70);

        card_title.text(title);
        card_text.text(summary);
        anchors.attr("href", link);
        //$('div.carousel-inner div:nth-child('+(indx+1)+')').attr("style","background-image: url(\'"+ featured_posts[indx].img +"\')" )

    });

    carousel.carousel();

}

});


