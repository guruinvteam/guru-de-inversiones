$( document ).ready(function() {

  var order_by = $("#order_by");

  init_search_bar("#search-input");
  init_search_bar("#search-res-input");


  order_by.on('change', function() {
    var results_list = $("#res_section");
    //results_list.children('hr').remove();
    var results = results_list.children('.card');


    //var updated = new Date(results.get(1).querySelector(".date").getAttribute("data-updated")); console.log(updated);

    var sorted = results.get();
    if (this.value == "latest"){
      sorted.sort(date_compare);
    } else if (this.value == "oldest"){
      sorted.reverse(date_compare);
    }else {
      console.log("nothing to sort");
      return;
    }

  console.log("sorting");
  results_list.children().remove();
  for (var i = 0; i < sorted.length; i++) {
    results_list.append(sorted[i]);
    results_list.append(document.createElement("hr"));
  }

    //results_list.append();

  });


function date_compare(a, b) {
  var a_date = new Date(a.querySelector(".date").getAttribute("data-updated"));
  var b_date = new Date(b.querySelector(".date").getAttribute("data-updated"));
  if (a_date > b_date)
    return -1;
  if (a_date < b_date)
    return 1;
  return 0;
}

});
