var express = require("express"),
    expressSanitizer = require("express-sanitizer"),
    bodyParser = require("body-parser");

var indexRoutes = require("./routes/index");

var app = express();

app.use(bodyParser.urlencoded({extended: true}));
app.use(expressSanitizer());

app.use(express.static(__dirname + '/public/'));
app.use(express.static(__dirname + '/views/posts/'));

app.set("view engine", "ejs");

app.use(indexRoutes);


app.listen(process.env.PORT, process.env.IP, function(){
    console.log("Guru de Inversiones Server Started...");
});
