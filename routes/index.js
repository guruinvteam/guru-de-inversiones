var express = require("express");
var rss = require("rss");
var router = express.Router();

var posts_info = require("../posts_info");
var posts = posts_info.posts;


var guru_url = "";
//"http://pepe-cs50-jose1791.c9users.io:8080";
//"https://safe-chamber-25415.herokuapp.com";
//require('os').hostname();

function populate_posts(ids_array) {
    var ret_array = [];

    ids_array.forEach(function(post_id) {
        ret_array.push(posts[post_id]);
    });

    return ret_array;
}

function load_index_posts(){

    var ind_posts = {
        "featured_posts": populate_posts(posts_info.feat_posts_ids),
        "sponsored_posts": populate_posts(posts_info.spon_posts_ids),
        "popular_posts": populate_posts(posts_info.pop_posts_ids),
        "latest_posts": populate_posts(posts_info.latest_posts_ids)
    };

    return ind_posts;
}

// compare last updated function to sort found_posts array
function post_date_compare(a, b) {
    if (a.updated > b.updated)
      return -1;
    if (a.updated < b.updated)
      return 1;
    return 0;
}


function search_posts_all(query, reverse) {
    var found_posts = [];
    query = query.toLowerCase();

    for (let post_key in posts) {
        var found = false;
        let post = posts[post_key];

        //title
        if (post.title.toLowerCase().indexOf(query) !== -1) {
            found_posts.push(post);
            continue;
        }

        //categories
        for (var i = 0; i < post.categories.length; i++) {
            if (post.categories[i].toLowerCase().indexOf(query) !== -1){
                found_posts.push(post);
                found = true;
                break;
            }
        }
        if (found) { continue; }

        // tags
        for (var i = 0; i < post.tags.length; i++) {
            if (post.tags[i].toLowerCase().indexOf(query) !== -1){
                found_posts.push(post);
                found = true;
                break;
            }
        }
        if (found) { continue; }

        // summary
        if (post.summary.toLowerCase().indexOf(query) !== -1) {
            found_posts.push(post);
            found = true;
            continue;
        }
        if (found) { continue; }
    }

    if(!reverse) {
        // return sorted array (last updated posts first)
        return found_posts.sort(post_date_compare);
    } else {
        // return sorted array (oldest posts first)
        return found_posts.reverse(post_date_compare);
    }

}

function search_posts(key_value, reverse){
    var found_posts = [];
    var value, key;
    if ("tag" in key_value) {
        key = "tags";
        value = key_value["tag"];
    } else if ("category" in key_value) {
        key = "categories";
        value = key_value["category"];
    } else if ("all" in key_value) {
        var query = key_value["all"];
        return search_posts_all(query, reverse);
    } else {
        return [];
    }

    for (let post_key in posts) {
        let post = posts[post_key];

        if(post[key].indexOf(value) != -1) {
            found_posts.push(post);
        }
    }

    if(!reverse) {
        // return sorted array (last updated posts first)
        return found_posts.sort(post_date_compare);
    } else {
        // return sorted array (oldests posts first)
        return found_posts.reverse(post_date_compare);
    }


}

var index_posts = load_index_posts();


router.get("/", function(req, res) {

    var page_data = {
        "posts": index_posts
    };

    res.render("index.ejs", data = page_data);
});



router.get("/posts/:titleid", function(req, res) {

    var post_id = req.params.titleid;

    if (post_id in posts) {

        var page_data = {
            "post": posts[post_id]
        };

        res.render("posts/" + post_id + "/" + post_id + ".ejs", data = page_data);
    } else {
        res.send("Resource not found");
    }

});


router.get("/suggestions", function(req, res) {
    var found_posts = search_posts_all(req.query.q, false);

    res.send(found_posts);
});


router.get("/search/:type", function(req, res) {
    var search_type = req.params.type;
    var search_val = req.query.q;
    var query_info = {};

    query_info[search_type] = search_val;

    //console.log("type", search_type, "val", search_val, "info", query_info);

    var page_data = {
        "found_posts": search_posts(query_info, false)
    };

    res.render("search_results.ejs", data = page_data);

});


router.get("/rss", function(req, res) {
    // TODO temp workaround for testing
    guru_url = req.get('host');

    var feed = new rss({
        title: 'guru de inversiones',
        description: 'guru ide inversiones short description here',
        feed_url: guru_url + '/rss',
        site_url: guru_url,
        image_url:  guru_url + '/icon.png',
        //docs: guru_url + '/rss/docs.html',
        managingEditor: 'FerSedano@gmail.com (Fer Sedano)',
        webMaster: 'PepeChavez@gmail.com (Pepe Chavez)',
        copyright: '2018 Guru de Inversiones',
        language: 'es',
        categories: ['Category 1','Category 2','Category 3'],
        pubDate: 'May 20, 2018 04:00:00 GMT',
        ttl: '60'
    });

    for (let post_key in posts) {
        let post = posts[post_key];

        feed.item({ "title": post.title,
                    "description": post.summary,
                    "url" : guru_url + post.route,
                    "categories" : post.categories,
                    "author": post.author,
                    "date": post.updated
                    });
    }

    var xml = feed.xml({indent: true});
    res.set('Content-Type', 'application/rss+xml');
    res.send(xml);
});


var links = {
    faq: { es: { title: "FAQ", content: ""} },
    terms: { es: { title: "Terminos y Condiciones", content: ""} },
    privacy: { es: { title: "Politica de Privacidad", content: ""} },
    about: { es: { title: "Acerca de...", content: ""} }
};

router.get("/:link", function(req, res) {
    var link = req.params.link;

    if (link in links) {
        res.render("link.ejs", data = links[link]["es"]);
    } else {
        res.send("Invalid route");
    }
});

router.get("*", function(req, res) {
    res.send("Invalid route");
});


module.exports = router;